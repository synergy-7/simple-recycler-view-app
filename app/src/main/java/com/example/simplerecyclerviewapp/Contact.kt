package com.example.simplerecyclerviewapp

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Contact(
    val name: String,
    val phoneNumber: Int
) : Parcelable

val contacts = arrayListOf<Contact>(
    Contact("Arif", 123),
    Contact("Andrea", 456),
    Contact("Hirata", 789)
)

val alphabets = ('A'..'Z').map { it.toString() }.toMutableList()

enum class Words(val char: String, val words: ArrayList<String>){
    A("A", arrayListOf("Arif", "Andrea")),
    B("B", arrayListOf("Bandung", "Biawak")),
    C("C", arrayListOf("Cantik", "Cerdas"))
}
